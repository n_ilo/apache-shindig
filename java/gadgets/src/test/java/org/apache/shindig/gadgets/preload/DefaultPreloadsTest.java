/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.preload;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class DefaultPreloadsTest {

  private DefaultPreloads preloads;
  private Map<String, PreloadedData> preloadedDataMap;

  @Before
  public void setUp() {
    preloads = new DefaultPreloads();
    preloadedDataMap = newHashMap();
  }

  @Test
  public void getKeys() {
    preloadedDataMap.put("foo", new TestPreloadedData());
    preloadedDataMap.put("bar", null);

    preloads.putAll(preloadedDataMap);

    Set<String> expectedKeys = newHashSet("foo", "bar");
    assertThat(preloads.getKeys(), is(expectedKeys));
  }

  @Test
  public void getPreloadedData() throws Exception {
    preloadedDataMap.put("foo", new TestPreloadedData());

    preloads.putAll(preloadedDataMap);

    assertNotNull(preloads.getData("foo"));
    assertThat(preloads.getData("foo").toJson().toString(), is("value for foo"));
  }

  private class TestPreloadedData implements PreloadedData {
    public Object toJson() {
      return "value for foo";
    }
  }
}
